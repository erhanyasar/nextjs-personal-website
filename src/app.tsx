import { CircularProgress } from "@mui/material";
import type { Metadata } from "next";
import Error from "next/error";
import { useState } from "react";
import { useUserInfo } from "../pages/api/useSWR";
import WelcomeDialog from "./components/dialogs/welcomeDialog";
import Footer from "./components/layout/footer";
import { Header } from "./components/layout/header";
import AboutMe from "./components/sections/aboutMe";
import Blog from "./components/sections/blog";
import Chronology from "./components/sections/chronology/chronology";
import ContactMe from "./components/sections/contactMe";
import Portfolio from "./components/sections/portfolio/portfolio";
import References from "./components/sections/references";
import UserInfo from "./components/sections/userInfo";

export const metadata: Metadata = {
  title: "Erhan Yaşar",
  description:
    " | MSc., Frontend Architect, Software Instructor, Community Lead, Mentor",
  appleWebApp: {
    title: "Erhan Yaşar",
  },
};

export const viewport = {
  width: "device-width",
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};

export default function App(): JSX.Element {
  const [openSection, setOpenSection] = useState<string>("");
  const [isWelcomeDialogOpen, setIsWelcomeDialogOpen] = useState<boolean>(false);

  const { data, error, isLoading } = useUserInfo();

  if (error) return <Error statusCode={error.status ?? error.message} />;
  if (isLoading) return <CircularProgress style={{ margin: "35% 50%" }} />;
  if (!data) return <></>;

  const { welcomeDialog, user, chronology, references, contact } = data;

  return (
    <>
      <style jsx global>
        {`
          section { padding: 25px; }
          #userInfo { padding: 50px 25px 0; }
        `}
      </style>

      <Header title={user.title} setOpenSection={setOpenSection} />

      <WelcomeDialog
        isOpen={isWelcomeDialogOpen}
        onClose={setIsWelcomeDialogOpen}
        data={welcomeDialog}
      />

      <UserInfo userInfo={user.info} />
      <AboutMe openSection={openSection} setOpenSection={setOpenSection} aboutMe={user.aboutMe} />
      <Chronology
        openSection={openSection}
        setOpenSection={setOpenSection}
        chronology={chronology}
      />
      <Portfolio openSection={openSection} setOpenSection={setOpenSection} />
      <References
        openSection={openSection}
        setOpenSection={setOpenSection}
        references={references}
      />
      {false && <Blog />}
      <ContactMe openSection={openSection} setOpenSection={setOpenSection} contacts={contact} />

      <Footer />
    </>
  );
}
