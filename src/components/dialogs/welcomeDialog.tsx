import { useState, SetStateAction, Dispatch, forwardRef } from "react";
import {
  DialogActions,
  DialogContent,
  Button,
  DialogTitle,
  Slide,
  Typography,
} from "@mui/material";
import Dialog, { DialogProps } from "@mui/material/Dialog";
import { TransitionProps } from "@mui/material/transitions";
import { styled } from "@mui/material/styles";

const Transition = forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const CustomTypography = styled(Typography)(() => ({
  margin: "10px 0px 20px 10px",
  color: "rgba(0, 0, 0, 0.6)",
}));

export default function WelcomeDialog({
  isOpen,
  onClose,
  data,
}: {
  isOpen: boolean;
  onClose: Dispatch<SetStateAction<boolean>>;
  data: string[];
}): JSX.Element {
  const [fullWidth] = useState(true);
  const [maxWidth] = useState<DialogProps["maxWidth"]>("sm");

  const handleClose = (): void => onClose(false);

  return (
    <>
      <style jsx global>{`
        .styledPhrases {
          color: #1f75cb;
          background-color: transparent;
        }
        .dialogContent {
          padding: 10px 15px;
        }
      `}</style>
      <Dialog
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        open={isOpen}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <DialogTitle className="styledPhrases">
          {data ? data[0] : null}
        </DialogTitle>
        <DialogContent className="dialogContent">
          {data.map((each, index) => {
            if (index + 1 === undefined) return;
            return (
              <CustomTypography
                key={index}
                dangerouslySetInnerHTML={{ __html: data[index + 1] }}
              ></CustomTypography>
            );
          })}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
