import DownloadIcon from "@mui/icons-material/Download";
import { Fab, Stack } from "@mui/material";
import { useSnackbar } from "notistack";
import DownloadFile from "../../utils/helpers/downloadFile";

export default function DownloadResumeFAB(): JSX.Element {
  const { enqueueSnackbar } = useSnackbar();

  const handleDownloadResume = (): void => {
    try {
      DownloadFile("Resume");
      enqueueSnackbar("File downloaded successfully!", { variant: "success" });
    } catch {
      enqueueSnackbar("There's an error while downloading the file!", { variant: "error" });
    }
  };

  return (
    <Stack direction="row" justifyContent="flex-end" p={5}>
      <Fab
        color="secondary"
        aria-label="Download My Resume"
        title="Download My Resume"
        onClick={handleDownloadResume}
      >
        <DownloadIcon />
      </Fab>
    </Stack>
  );
}
