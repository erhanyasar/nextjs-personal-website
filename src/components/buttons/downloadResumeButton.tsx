import DownloadIcon from "@mui/icons-material/Download";
import { Button, Stack } from "@mui/material";
import { useSnackbar } from "notistack";
import DownloadFile from "../../utils/helpers/downloadFile";

export default function DownloadResumeButton(): JSX.Element {
  const { enqueueSnackbar } = useSnackbar();

  const handleDownload = (param: string): void => {
    try {
      DownloadFile(param);
      enqueueSnackbar("File downloaded successfully!", { variant: "success" });
    } catch {
      enqueueSnackbar("There's an error while downloading the file!", { variant: "error" });
    }
  };

  return (
    <Stack direction="row" justifyContent="center" p={5} gap={5}>
      <Button
        variant="outlined"
        color="secondary"
        startIcon={<DownloadIcon />}
        onClick={() => handleDownload("Resume")}
      >
        Download Resume
      </Button>
      <Button
        variant="outlined"
        color="primary"
        startIcon={<DownloadIcon />}
        onClick={() => handleDownload("CV")}
      >
        Download CV (Detailed)
      </Button>
    </Stack>
  );
}
