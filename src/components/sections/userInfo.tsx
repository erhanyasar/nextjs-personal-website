import { Divider, Typography } from "@mui/material";
import { UserInfoProps } from "../../types/sections";
import DownloadResumeFAB from "../buttons/downloadResumeFAB";

export default function UserInfo({ userInfo }: UserInfoProps): JSX.Element {
  return (
    <>
      <section id="userInfo">
        <Typography
          variant="h1"
          component="h2"
          dangerouslySetInnerHTML={{ __html: userInfo }}
        ></Typography>
        <DownloadResumeFAB />
      </section>
      <Divider />
    </>
  );
}
