import { Box, Tab, Tabs, tabsClasses } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { SyntheticEvent, useState } from "react";
import SwipeableViews from "react-swipeable-views";
import { TabPanelProps } from "../../../types/portfolio";
import { PORTFOLIO_TABS } from "../../../utils/constants";
import Animation from "./tabs/animation";
import Design from "./tabs/design";
import Educative from "./tabs/educative";
import EventTab from "./tabs/event";
import Frontend from "./tabs/frontend";
import Fullstack from "./tabs/fullstack";
import GenerativeArt from "./tabs/generativeArt";
import Modeling from "./tabs/modeling";
import Photography from "./tabs/photography";
import Presentation from "./tabs/presentation";
import Publication from "./tabs/publication";
import XR from "./tabs/xr";

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      style={{ marginTop: "24px" }}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

export default function MobilePortfolioTabs(): JSX.Element {
  const theme = useTheme();
  const [value, setValue] = useState<number>(0);

  const handleChange = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  return (
    <Box sx={{ bgcolor: "background.dark", width: 500 }}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="secondary"
        textColor="secondary"
        variant="scrollable"
        scrollButtons
        allowScrollButtonsMobile
        aria-label="Mobile portfolio tabs"
        sx={{
          [`& .${tabsClasses.scrollButtons}`]: {
            "&.Mui-disabled": { opacity: 0.3 },
          },
        }}
      >
        {PORTFOLIO_TABS.map((tab, index) => {
          return <Tab key={index} label={tab} {...a11yProps(index)} />;
        })}
      </Tabs>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <GenerativeArt isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <Educative isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <Frontend isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}>
          <Fullstack isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={4} dir={theme.direction}>
          <XR isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={5} dir={theme.direction}>
          <EventTab isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={6} dir={theme.direction}>
          <Presentation isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={7} dir={theme.direction}>
          <Design isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={8} dir={theme.direction}>
          <Animation isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={9} dir={theme.direction}>
          <Publication isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={10} dir={theme.direction}>
          <Photography isMobile={true} />
        </TabPanel>
        <TabPanel value={value} index={11} dir={theme.direction}>
          <Modeling isMobile={true} />
        </TabPanel>
      </SwipeableViews>
    </Box>
  );
}
