import { Box, CircularProgress, Divider, Stack, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { usePortfolio } from "../../../../pages/api/useSWR";
import { PortfolioProps } from "../../../types/sections";
import CollapseIcon from "../../icons/accordion/collapseIcon";
import ExpandIcon from "../../icons/accordion/expandIcon";
import MobilePortfolioTabs from "./mobilePortfolioTabs";
import VerticalPortfolioTabs from "./verticalPortfolioTabs";

export default function Portfolio({ openSection, setOpenSection }: PortfolioProps): JSX.Element {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("sm"));

  const { data, error, isLoading } = usePortfolio();

  if (error) return <div>Failed to load</div>;
  if (isLoading) return <CircularProgress />;
  if (!data) return null;

  const handleToggle = (): void => {
    if (openSection === "portfolio") setOpenSection("");
    else setOpenSection("portfolio");
  };

  return (
    <>
      <section id="portfolio">
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
          onClick={handleToggle}
        >
          <Box>
            <Typography variant="h2" component="h2">
              Portfolio
            </Typography>
            <Typography className="heading"></Typography>
          </Box>
          {openSection !== "portfolio" ? <ExpandIcon /> : <CollapseIcon />}
        </Stack>
        {openSection === "portfolio" &&
          (matches ? <MobilePortfolioTabs /> : <VerticalPortfolioTabs />)}
      </section>
      <Divider />
    </>
  );
}
