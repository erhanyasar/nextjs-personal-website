import { Box, Tab, Tabs } from "@mui/material";
import { SyntheticEvent, useState } from "react";
import { TabPanelProps } from "../../../types/portfolio";
import { PORTFOLIO_TABS } from "../../../utils/constants";
import Animation from "./tabs/animation";
import Design from "./tabs/design";
import Educative from "./tabs/educative";
import EventTab from "./tabs/event";
import Frontend from "./tabs/frontend";
import Fullstack from "./tabs/fullstack";
import GenerativeArt from "./tabs/generativeArt";
import Modeling from "./tabs/modeling";
import Photography from "./tabs/photography";
import Presentation from "./tabs/presentation";
import Publication from "./tabs/publication";
import XR from "./tabs/xr";

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      style={{ width: "100%", overflow: "auto" }}
      {...other}
    >
      {value === index && <Box sx={{ p: 3, pt: 0 }}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

export default function VerticalPortfolioTabs(): JSX.Element {
  const [value, setValue] = useState<number>(0);

  const handleChange = (event: SyntheticEvent, newValue: number) => setValue(newValue);

  return (
    <Box
      sx={{
        flexGrow: 1,
        bgcolor: "background.paper",
        display: "flex",
        width: "100%",
      }}
      py={5}
      px={2}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        textColor="secondary"
        indicatorColor="secondary"
        aria-label="Vertical portfolio tabs for desktop"
        sx={{ borderRight: 1, borderColor: "divider", overflow: "visible" }}
      >
        {PORTFOLIO_TABS.map((tab, index) => {
          return <Tab key={index} label={tab} {...a11yProps(index)} />;
        })}
      </Tabs>
      <TabPanel value={value} index={0}>
        <GenerativeArt />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Educative />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Frontend />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Fullstack />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <XR />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <EventTab />
      </TabPanel>
      <TabPanel value={value} index={6}>
        <Presentation />
      </TabPanel>
      <TabPanel value={value} index={7}>
        <Design />
      </TabPanel>
      <TabPanel value={value} index={8}>
        <Animation />
      </TabPanel>
      <TabPanel value={value} index={9}>
        <Publication />
      </TabPanel>
      <TabPanel value={value} index={10}>
        <Photography />
      </TabPanel>
      <TabPanel value={value} index={11}>
        <Modeling />
      </TabPanel>
    </Box>
  );
}
