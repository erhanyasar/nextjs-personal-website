import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import {
  Box,
  IconButton,
  ImageList,
  ImageListItem,
  ImageListItemBar,
} from "@mui/material";
import { usePortfolio } from "../../../../../pages/api/useSWR";

export default function Publication({
  isMobile,
}: {
  isMobile?: boolean;
}): JSX.Element {
  const { data } = usePortfolio();

  const { portfolio } = data ?? {};

  return (
    <Box sx={{ mt: 0, width: "95%", height: "95%", marginLeft: "5%" }}>
      <ImageList cols={isMobile ? 1 : 4} gap={8}>
        {portfolio?.publicationData.map((item) => (
          <ImageListItem
            key={item.img}
            sx={{ cursor: item.url ? "pointer" : "cursor" }}
            onClick={() => (item.url ? window.open(item.url) : null)}
          >
            <img
              src={`${item.img}?w=248&fit=crop&auto=format`}
              srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={item.title}
              title={item.title}
              loading="lazy"
            />
            <ImageListItemBar
              title={item.title}
              subtitle={item.description}
              actionIcon={
                <IconButton
                  sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                  aria-label={`info about ${item.description}`}
                  title={item.title}
                >
                  <PlayCircleIcon />
                </IconButton>
              }
            />
          </ImageListItem>
        ))}
      </ImageList>
    </Box>
  );
}
