import { Box, ImageList, ImageListItem, ImageListItemBar } from "@mui/material";
import { usePortfolio } from "../../../../../pages/api/useSWR";

export default function Modeling({
  isMobile,
}: {
  isMobile?: boolean;
}): JSX.Element {
  const { data } = usePortfolio();

  const { portfolio } = data ?? {};

  return (
    <Box sx={{ width: "95%", height: "95%", marginLeft: "5%" }}>
      <ImageList cols={isMobile ? 1 : 4} gap={8}>
        {portfolio?.modelingData.map((item) => (
          <ImageListItem key={item.img}>
            <img
              src={`${item.img}?w=248&fit=crop&auto=format`}
              srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={item.title}
              loading="lazy"
            />
            <ImageListItemBar title={item.title} subtitle={item.description} />
          </ImageListItem>
        ))}
      </ImageList>
    </Box>
  );
}
