import { Box, Divider, Stack, Typography } from "@mui/material";
import Image from "next/image";
import { ReferencesProps } from "../../types/sections";
import CollapseIcon from "../icons/accordion/collapseIcon";
import ExpandIcon from "../icons/accordion/expandIcon";

export default function References({ openSection, setOpenSection, references }: ReferencesProps): JSX.Element {
  const handleToggle = (): void => {
    if (openSection === "references") setOpenSection("");
    else setOpenSection("references");
  };

  return (
    <>
      <section id="references">
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
          onClick={handleToggle}
        >
          <Box>
            <Typography variant="h2" component="h2">
              References
            </Typography>
            <Typography className="heading"></Typography>
          </Box>
          {openSection !== "references" ? <ExpandIcon /> : <CollapseIcon />}
        </Stack>

        {openSection === "references" && (
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            sx={{ overflowX: "auto" }}
            spacing={5}
          >
            {references.map((reference) => {
              return (
                <Image
                  key={reference.URL}
                  src={reference.src}
                  alt={reference.title}
                  title={reference.title}
                  style={{ cursor: "pointer" }}
                  onClick={() => window.open(reference.URL)}
                  width={
                    reference.title === "Naval Academy" ||
                    reference.title === "Turkish Naval Forces"
                      ? 200
                      : 100
                  }
                  height={
                    reference.title === "Naval Academy" ||
                    reference.title === "Turkish Naval Forces"
                      ? 100
                      : 50
                  }
                />
              );
            })}
          </Stack>
        )}
      </section>
      <Divider />
    </>
  );
}
