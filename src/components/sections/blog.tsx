import { Typography, Divider, Stack, Box } from "@mui/material";
import ExpandIcon from "../icons/accordion/expandIcon";
import { useRouter } from "next/navigation";

export default function Blog(): JSX.Element {
  const router = useRouter();

  const handleRedirectBlogRoute = () => {
    if (confirm("Confirm redirecting to a different route?"))
      router.push("blog");
  };

  return (
    <>
      <section id="blog">
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
          onClick={handleRedirectBlogRoute}
        >
          <Box>
            <Typography variant="h2" component="h2">
              Blog
            </Typography>
            <Typography className="heading"></Typography>
          </Box>
          <ExpandIcon />
        </Stack>
      </section>
      <Divider />
    </>
  );
}
