import { Box, Divider, Stack, Typography } from "@mui/material";
import { AboutMeProps } from "../../types/sections";
import DownloadResumeButton from "../buttons/downloadResumeButton";
import CollapseIcon from "../icons/accordion/collapseIcon";
import ExpandIcon from "../icons/accordion/expandIcon";

export default function AboutMe({ openSection, setOpenSection, aboutMe }: AboutMeProps): JSX.Element {
  const handleToggle = (): void => {
    if (openSection === "aboutMe") setOpenSection("");
    else setOpenSection("aboutMe");
  };

  return (
    <>
      <section id="aboutMe">
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
          onClick={handleToggle}
        >
          <Box>
            <Typography variant="h2" component="h2">
              About me
            </Typography>
            <Typography className="heading"></Typography>
          </Box>
          {openSection !== "aboutMe" ? <ExpandIcon /> : <CollapseIcon />}
        </Stack>
        <Stack direction="column">
          {openSection === "aboutMe" && (
            <>
              {aboutMe.map((element, id) => {
                return (
                  <Stack key={id} direction="row" spacing={2}>
                    <Typography key={id} variant="body1" sx={{ padding: "10px 0" }}>
                      {element?.icon}
                    </Typography>
                    <Typography variant="body1" sx={{ padding: "10px 0" }}>
                      {element?.text}
                    </Typography>
                  </Stack>
                );
              })}
              <DownloadResumeButton />
            </>
          )}
        </Stack>
      </section>
      <Divider />
    </>
  );
}