import { Box, Divider, Stack, Typography } from "@mui/material";
import { ChronologyProps } from "../../../types/sections";
import DownloadResumeButton from "../../buttons/downloadResumeButton";
import CollapseIcon from "../../icons/accordion/collapseIcon";
import ExpandIcon from "../../icons/accordion/expandIcon";
import ChronologicResume from "./chronologicResume";

export default function Chronology({ openSection, setOpenSection, chronology }: ChronologyProps): JSX.Element {
  const handleToggle = (): void => {
    if (openSection === "chronology") setOpenSection("");
    else setOpenSection("chronology");
  };

  return (
    <>
      <section id="chronology">
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
          onClick={handleToggle}
        >
          <Box>
            <Typography variant="h2" component="h2">
              Chronology
            </Typography>
            <Typography className="heading"></Typography>
          </Box>
          {openSection !== "chronology" ? <ExpandIcon /> : <CollapseIcon />}
        </Stack>
        {openSection === "chronology" && (
          <div style={{ marginLeft: "-10vw" }}>
            <ChronologicResume chronology={chronology} />
            <DownloadResumeButton />
          </div>
        )}
      </section>
      <Divider />
    </>
  );
}
