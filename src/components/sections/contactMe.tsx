import EmailIcon from "@mui/icons-material/Email";
import FileCopyIcon from '@mui/icons-material/FileCopy';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import PhoneIcon from "@mui/icons-material/Phone";
import { Box, Grid, IconButton, Stack, Typography } from "@mui/material";
import { useSnackbar } from "notistack";
import { ContactMeProps } from "../../types/sections";
import DownloadFile from "../../utils/helpers/downloadFile";
import CollapseIcon from "../icons/accordion/collapseIcon";
import ExpandIcon from "../icons/accordion/expandIcon";
import GithubIcon from "../icons/portfolio/githubIcon";
import GitlabIcon from "../icons/portfolio/gitlabIcon";
import StackexchangeIcon from "../icons/portfolio/stackexchangeIcon";
import StackoverflowIcon from "../icons/portfolio/stackoverflowIcon";

export default function ContactMe({ openSection, setOpenSection, contacts }: ContactMeProps): JSX.Element {
  const { enqueueSnackbar } = useSnackbar();

  const handleToggle = (): void => {
    if (openSection === "contactMe") setOpenSection("");
    else setOpenSection("contactMe");
  };

  const handleDownload = (param: string): void => {
    try {
      DownloadFile(param);
      enqueueSnackbar("File downloaded successfully!", { variant: "success" });
    } catch {
      enqueueSnackbar("There's an error while downloading the file!", { variant: "error" });
    }
  };

  return (
    <section id="contactMe">
      <Stack
        direction="row"
        spacing={2}
        alignItems="center"
        justifyContent="space-between"
        onClick={handleToggle}
      >
        <Box>
          <Typography variant="h2" component="h2">
            Contact me
          </Typography>
          <Typography className="heading"></Typography>
        </Box>
        {openSection !== "contactMe" ? <ExpandIcon /> : <CollapseIcon />}
      </Stack>

      {openSection === "contactMe" && (
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <Stack direction="column" spacing={1} alignItems="center">
                <Typography variant="h5" component="h2">
                  Instant Reach
                </Typography>
                <Stack direction="column" spacing={2} alignItems="flex-start">
                  <Stack direction="row" spacing={2} alignItems="center">
                    <IconButton
                      aria-label="Call Mobile"
                      title="Call Mobile"
                      onClick={() => window.open(`tel:${contacts.phone}`, "_self")}
                    >
                      <PhoneIcon color="secondary" />
                    </IconButton>
                    <span>+90 533 494 0 400</span>
                  </Stack>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <IconButton
                      aria-label="Instant E-mail"
                      title="Instant E-mail"
                      onClick={() => window.open(`mailto:${contacts.email}`, "_self")}
                    >
                      <EmailIcon color="secondary" />
                    </IconButton>
                    <span>mail@erhanyasar.com.tr</span>
                  </Stack>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Stack direction="column" spacing={2} alignItems="center">
                <Typography variant="h5" component="h2">
                  Portfolio
                </Typography>
                <Stack direction="row" spacing={2} alignItems="flex-start">
                  <IconButton
                    aria-label="GitLab"
                    title="GitLab"
                    onClick={() => window.open(contacts.gitlab)}
                  >
                    <GitlabIcon />
                  </IconButton>
                  <IconButton
                    aria-label="GitHub"
                    title="GitHub"
                    onClick={() => window.open(contacts.github)}
                  >
                    <GithubIcon isFilled={true} />
                  </IconButton>
                  <IconButton
                    aria-label="Stackoverflow"
                    title="Stackoverflow"
                    onClick={() => window.open(contacts.stackoverflow)}
                  >
                    <StackoverflowIcon />
                  </IconButton>
                  <IconButton
                    aria-label="UX Stackexchange"
                    title="UX Stackexchange"
                    onClick={() => window.open(contacts.stackexchange)}
                  >
                    <StackexchangeIcon />
                  </IconButton>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Stack direction="column" spacing={2} alignItems="center">
                <Typography variant="h5" component="h2">
                  Social Media
                </Typography>
                <Stack direction="row" spacing={2} alignItems="flex-start">
                <IconButton
                    aria-label="LinkedIn"
                    title="LinkedIn"
                    onClick={() => window.open(contacts.linkedin)}
                  >
                    <LinkedInIcon color="secondary" />
                  </IconButton>
                  <IconButton
                    aria-label="Instagram"
                    title="Instagram"
                    onClick={() => window.open(contacts.instagram)}
                  >
                    <InstagramIcon color="secondary" />
                  </IconButton>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Stack direction="column" spacing={2} alignItems="center">
                <Typography variant="h5" component="h2">
                  Download
                </Typography>
                <Stack direction="row" spacing={2} alignItems="flex-start">
                  <IconButton
                    aria-label="Download Resume"
                    title="Download Resume"
                    onClick={() => handleDownload("Resume")}
                  >
                    <InsertDriveFileIcon color="secondary" />
                  </IconButton>
                  <IconButton
                    aria-label="Download CV"
                    title="Download CV"
                    onClick={() => handleDownload("CV")}
                  >
                    <FileCopyIcon color="secondary" />
                  </IconButton>
                </Stack>
              </Stack>
            </Grid>
          </Grid>
        </Box>
      )}
    </section>
  );
}
