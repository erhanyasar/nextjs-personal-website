import { Stack, Typography } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import Head from "next/head";
import { v4 as uuidv4 } from "uuid";
import { HeaderProps } from "../../types/layout";
import { SECTIONS } from "../../utils/constants";

export const Header: React.FC<HeaderProps> = ({ title, setOpenSection }) => {
  const matches = useMediaQuery("(min-width:600px)");

  return (
    <header>
      <style jsx scoped>{`
        header {
          position: relative;
          top: 0px;
          width: 100%;
          padding: 10px;
          padding-right: 50px;
          display: flex;
          justify-content: end;
          background-color: #2b90d9;
          color: white;
        }
        .header-nav {
          color: white;
          text-decoration: none;
          margin: 0 1rem;
          cursor: pointer;
          transition: 0.3s;
        }
      `}</style>
      <Head>
        <title>{title}</title>
        <meta property="og:charset" charSet="utf-8" key="charset" />
        <meta
          property="og:http-equiv"
          http-equiv="X-UA-Compatible"
          content="IE=edge"
          key="http-equiv"
        />
        <meta
          property="og:viewport"
          name="viewport"
          content="width=device-width, initial-scale=1"
          key="viewport"
        />
        <meta
          property="og:description"
          content={title}
          key="description"
        />
      </Head>
      <Stack direction="row">
        {matches && (
          <>
            {SECTIONS.map((section) => (
              <a href={`#${section.url}`} className="header-nav" key={uuidv4()}>
                <Typography
                  variant="body1"
                  component="span"
                  onClick={() => setOpenSection(section.url)}
                >
                  {section.name}
                </Typography>
              </a>
            ))}
          </>
        )}
      </Stack>
    </header>
  );
};
