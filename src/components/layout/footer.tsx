export default function Footer(): JSX.Element {
  return (
    <>
      <style jsx scoped>{`
        footer {
          position: relative;
          bottom: 0px;
          width: 100%;
          height: 5px;
          // padding: 10px 5px;
          background-color: #ef5285;
          color: white;
        }
      `}</style>
      <footer></footer>
    </>
  );
}
