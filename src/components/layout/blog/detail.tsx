export default function Detail() {
  return (
    <>
      <style jsx global>{`
        section {
          display: inline-block;
          min-height: 91vh;
          width: 65%;
        }
      `}</style>
      <section>Detail</section>
    </>
  );
}
