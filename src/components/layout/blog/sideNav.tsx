export default function SideNav() {
  return (
    <>
      <style jsx global>{`
        aside.side-nav {
          display: inline-block;
          min-height: 91vh;
          width: 25%;
          border-right: 1px solid #ef5285;
        }
      `}</style>
      <aside className="side-nav">Side Navigation</aside>
    </>
  );
}
