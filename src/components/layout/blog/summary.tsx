export default function Summary() {
  return (
    <>
      <style jsx global>{`
        aside.summary {
          display: inline-block;
          position: relative;
          min-height: 91vh;
          border-left: 1px solid #ef5285;
        }
      `}</style>
      <aside className="summary">Summary</aside>
    </>
  );
}
