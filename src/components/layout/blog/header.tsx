import { Button, IconButton, Stack, Tooltip } from "@mui/material";
import Link from "next/link";
import HomeIcon from "@mui/icons-material/Home";
import RssFeedIcon from "@mui/icons-material/RssFeed";

import { Inter } from "next/font/google";
import type { Metadata } from "next";

const inter = Inter({ subsets: ["latin"] });

export default function BlogHeader(): JSX.Element {
  return (
    <header>
      <style jsx scoped>{`
        header {
          padding: 10px;
          position: relative;
          width: 100%;
          top: 0px;
          border-bottom: 1px solid #379398;
        }
        .headerWrapper {
          margin: 0 25px;
          display: flex;
          justify-content: space-between;
        }
      `}</style>
      <div className="headerWrapper">
        <Link href="/blog">
          <Button
            variant="contained"
            title="Return Home"
            sx={{
              color: "white",
              backgroundColor: "#379398",
              borderRadius: "20px",
            }}
            startIcon={<HomeIcon />}
          >
            <div>Home</div>
          </Button>
        </Link>
        <Tooltip title="Sign for RSS Feed">
          <IconButton color="primary" aria-label="add to shopping cart">
            <RssFeedIcon aria-label="rss-feed" />
          </IconButton>
        </Tooltip>
      </div>
    </header>
  );
}
