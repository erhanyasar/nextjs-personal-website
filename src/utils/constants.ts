import { PortfolioTabs } from "../types/portfolio";
import { Sections } from "../types/sections";

export const PORTFOLIO_TABS: PortfolioTabs = [
  "Generative Art",
  "Educative",
  "Frontend",
  "Full Stack",
  "XR",
  "Event",
  "Presentation",
  "Design",
  "Animation",
  "Publication",
  "Photography",
  "Modeling",
];

export const SECTIONS: Sections[] = [
  { name: "About Me", url: "aboutMe" },
  { name: "Chronology", url: "chronology" },
  { name: "Portfolio", url: "portfolio" },
  { name: "References", url: "references" },
  { name: "Contact Me", url: "contactMe" },
];
