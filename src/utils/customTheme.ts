export const customTheme = {
  palette: {
    primary: {
      main: "#2b90d9",
    },
    secondary: {
      main: "#ef5285",
    },
    ternary: {
      main: "#379398",
    },
  },
};
