import download from "downloadjs";

const DownloadFile = (fileType: string): void => {
  const x: XMLHttpRequest = new XMLHttpRequest();

  x.open("GET", `/files/${fileType}_ErhanYasar.pdf`, true);
  x.responseType = "blob";
  x.onload = (): boolean | XMLHttpRequest =>
    download(x.response, `${fileType}_ErhanYasar.pdf`, "application/pdf");
  x.send();
};

export default DownloadFile;
