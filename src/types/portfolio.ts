import { ReactNode } from "react";

export type FrontendData = {
  img: string;
  title: string;
  description: string;
  path: string;
  gitURL: string;
  path2?: string;
  pathTitle?: string;
  path2Title?: string;
  rows: number;
  cols: number;
  featured?: boolean;
};

export type FullstackData = {
  img: string;
  title: string;
  description: string;
  path: string;
  gitURL: string;
  rows?: number;
  cols?: number;
  featured?: boolean;
};

export type GenerativeArtData = {
  img: string;
  title: string;
  description: string;
  path: string;
  gitURL: string;
  featured?: boolean;
};

export type XRData = {
  img: string;
  title: string;
  description: string;
  path: string;
  gitURL: string;
  rows?: number;
  cols?: number;
  featured?: boolean;
};

export type EventData = {
  img: string;
  title: string;
  alt: string;
  description: string;
  url: string;
  featured?: boolean;
};

export type PresentationData = {
  img: string;
  title: string;
  alt: string;
  description: string;
  url: string;
  featured?: boolean;
};

export type DesignData = {
  img: string;
  title: string;
  description: string;
  url: string;
  rows?: number;
  cols?: number;
  featured?: boolean;
};

export type AnimationData = {
  img: string;
  title: string;
  description: string;
  url: string;
  featured?: boolean;
};

export type PublicationData = {
  img: string;
  title: string;
  description: string;
  url: string;
  rows?: number;
  cols?: number;
  featured?: boolean;
};

export type PhotographyData = {
  img: string;
  title: string;
  description: string;
};

export type ModelingData = {
  img: string;
  title: string;
  description: string;
};

export type TabPanelProps = {
  children?: ReactNode;
  dir?: string;
  index: number;
  value: number;
};

export type PortfolioTabs = string[];
