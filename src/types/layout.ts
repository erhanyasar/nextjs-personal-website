import { Dispatch, SetStateAction } from "react";

export type HeaderProps = {
  title: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
};
