import { Dispatch, SetStateAction } from "react";

import {
  AnimationData,
  DesignData,
  EventData,
  FrontendData,
  FullstackData,
  GenerativeArtData,
  ModelingData,
  PhotographyData,
  PresentationData,
  PublicationData,
  XRData,
} from "./portfolio";

export type Data = {
  welcomeDialog: string[];
  user: User;
  chronology: Chronology[];
  portfolio: Portfolio;
  references: Reference[];
  contact: Contact;
};

type User = {
  info: string;
  title: string;
  aboutMe: UserAboutMe[];
};

export type UserAboutMe = {
  text: string;
  icon: string;
};

export type Chronology = {
  date: string;
  topic?: string;
  detail?: string;
};

type Portfolio = {
  educativeData: FrontendData[];
  frontendData: FrontendData[];
  fullstackData: FullstackData[];
  generativeArtData: GenerativeArtData[];
  xrData: XRData[];
  eventData: EventData[];
  presentationData: PresentationData[];
  designData: DesignData[];
  animationData: AnimationData[];
  publicationData: PublicationData[];
  photographyData: PhotographyData[];
  modelingData: ModelingData[];
};

export type Reference = {
  src: string;
  alt: string;
  title: string;
  URL: string;
};

export type Contact = {
  phone: string;
  email: string;
  linkedin: string;
  gitlab: string;
  github: string;
  stackoverflow: string;
  stackexchange: string;
  behance: string;
  instagram: string;
  facebook: string;
  twitter: string;
};

export type Sections = {
  name: string;
  url: "aboutMe" | "chronology" | "portfolio" | "references" | "contactMe";
};

export type UserInfoProps = {
  userInfo: string;
}

export type AboutMeProps = {
  openSection: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
  aboutMe: UserAboutMe[];
}

export type ChronologyProps = {
  openSection: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
  chronology: Chronology[];
}

export type PortfolioProps = {
  openSection: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
}

export type ReferencesProps = {
  openSection: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
  references: Reference[];
}

export type ContactMeProps = {
  openSection: string;
  setOpenSection: Dispatch<SetStateAction<string>>;
  contacts: Contact;
}