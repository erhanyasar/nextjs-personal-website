export default function NotFound(): JSX.Element {
  return (
    <>
      <style jsx global>
        {`
          .buttonsWrapper {
            margin: 10px;
          }
        `}
      </style>
      <h2>Route Not Found!</h2>
      <button className="buttonsWrapper" onClick={() => history.back()}>Go back</button>
      <button className="buttonsWrapper" onClick={() => location.replace("/")}>Home Page</button>
    </>
  );
}
