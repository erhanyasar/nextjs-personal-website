import Head from "next/head";
import BlogHeader from "../../src/components/layout/blog/header";
import SideNav from "../../src/components/layout/blog/sideNav";
import Detail from "../../src/components/layout/blog/detail";
import Summary from "../../src/components/layout/blog/summary";
import Footer from "../../src/components/layout/footer";

export default function Blog() {
  return (
    <>
      <style jsx global>{`
        body {
          margin: 0;
          overflow: hidden;
        }
      `}</style>
      <Head>
        <title>{"Blog | Erhan Yaşar"}</title>
      </Head>
      <BlogHeader />
      <SideNav />
      <Detail />
      <Summary />
      <Footer />
    </>
  );
}
