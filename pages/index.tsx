import { CircularProgress, createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { BrowserTracing } from "@sentry/browser";
import * as Sentry from "@sentry/nextjs";
import { SpeedInsights } from "@vercel/speed-insights/next";
import { SnackbarProvider } from "notistack";
import { Suspense } from "react";
import TagManager from "react-gtm-module";
import App from "../src/app";
import { customTheme } from "../src/utils/customTheme";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

if (typeof window === "object")
  TagManager.initialize({
    gtmId: process.env.NEXT_PUBLIC_GTM_ID,
  });

const version: string = JSON.stringify(process.env.npm_package_version || "0.0.0");
const isSentryDebugModeActive = process.env.NODE_ENV === "development" ? false : true;

if (isSentryDebugModeActive)
  Sentry.init({
    dsn: process.env.NEXT_PUBLIC_SENTRY_DSN,
    integrations: [new BrowserTracing() as never /*, Sentry.replayIntegration() */],
    tracesSampleRate: process.env.NODE_ENV === "production" ? 1.0 : 0.1,
    environment: process.env.NODE_ENV,
    release: `erhanyasar-personal-website@${version}`,
    debug: isSentryDebugModeActive,
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
});


export default function Index(): JSX.Element {
  const theme = createTheme(customTheme);

  return (
    <Suspense fallback={<CircularProgress />}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <SnackbarProvider maxSnack={3}>
          <App />
          <SpeedInsights />
        </SnackbarProvider>
      </ThemeProvider>
    </Suspense>
  );
}
