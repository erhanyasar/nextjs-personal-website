export const contactQuery = {
  phone: "+905334940400",
  email: "mail@erhanyasar.com.tr",
  linkedin: "https://tr.linkedin.com/in/erhanyaşar",
  gitlab: "https://www.gitlab.com/erhanyasar",
  github: "https://github.com/erhanyasar",
  stackoverflow: "https://stackoverflow.com/users/6371094/erhan-yaşar",
  stackexchange: "https://ux.stackexchange.com/users/112317/erhan-yaşar",
  behance: "https://www.behance.net/erhanyasar",
  instagram: "http://www.instagram.com/erhan_yasar",
  facebook: "http://wwww.facebook.com/erhanyasar0",
  twitter: "http://www.twitter.com/erhan_yasar",
};