export const referencesQuery = [
  {
    src: "/img/reference/dho.png",
    title: "Naval Academy",
    URL: "https://dho.msu.edu.tr",
  },
  {
    src: "/img/reference/dzkk.png",
    title: "Turkish Naval Forces",
    URL: "https://www.dzkk.tsk.tr",
  },
  {
    src: "/img/reference/msgsu.png",
    title: "Mimar Sinan University of Fine Arts",
    URL: "http://www.msgsu.edu.tr",
  },
  {
    src: "/img/reference/arete.png",
    title: "Arete Consulting",
    URL: "http://www.arete.com.tr",
  },
  {
    src: "/img/reference/relateddigital.png",
    title: "Related Digital",
    URL: "https://www.relateddigital.com",
  },
  {
    src: "/img/reference/devtagon.jpg",
    title: "Devtagon",
    URL: "https://www.linkedin.com/company/devtagon-bili%C5%9Fim-ve-teknoloji-%C3%A7%C3%B6z%C3%BCmleri-a%C5%9F/?originalSubdomain=tr",
  },
  {
    src: "/img/reference/logo-bilisimegitim.png",
    title: "Bilişim Education Center",
    URL: "https://www.bilisimegitim.com",
  },
  {
    src: "/img/reference/intellectsoft-logo-blue.png",
    title: "Intellectsoft",
    URL: "https://www.intellectsoft.net",
  },
  {
    src: "/img/reference/testimonialtree.png",
    title: "Testimonial Tree",
    URL: "https://testimonialtree.com",
  },
  {
    src: "/img/reference/tb.svg",
    title: "Toshi Bet",
    URL: "https://toshi.bet/",
  },
  {
    src: "/img/reference/acunmedya-akademi.png",
    title: "Acunmedya Akademi",
    URL: "https://acunmedyaakademi.com/",
  },
];