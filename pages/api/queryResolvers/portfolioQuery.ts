import { animationData } from "./portfolio/animationData";
import { designData } from "./portfolio/designData";
import { educativeData } from "./portfolio/educativeData";
import { eventData } from "./portfolio/eventData";
import { frontendData } from "./portfolio/frontendData";
import { fullstackData } from "./portfolio/fullstackData";
import { generativeArtData } from "./portfolio/generativeArtData";
import { modelingData } from "./portfolio/modelingData";
import { photographyData } from "./portfolio/photographyData";
import { presentationData } from "./portfolio/presentationData";
import { publicationData } from "./portfolio/publicationData";
import { xrData } from "./portfolio/xrData";

export const portfolioQuery = {
  educativeData: educativeData,
  frontendData: frontendData,
  fullstackData: fullstackData,
  generativeArtData: generativeArtData,
  xrData: xrData,
  eventData: eventData,
  presentationData: presentationData,
  designData: designData,
  animationData: animationData,
  publicationData: publicationData,
  photographyData: photographyData,
  modelingData: modelingData,
};