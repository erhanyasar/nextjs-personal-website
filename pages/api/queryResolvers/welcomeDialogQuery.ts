export const welcomeDialogQuery = [
  `Welcome,`,
  `This website is developed by me from scratch with a Next.js TypeScript boilerplate to represent itself as a portfolio.`,
  `Firstly, I wanted to move everything as quickly as possible since I was not having any design in mind at the moment 
  I started. So that, in the meantime, it&apos;s better to observe day-to-day progress from the repo contributions either 
  under <b class="styledPhrases"> Selected works </b> or <b class="styledPhrases"> Contact me</b> section.`,
  `During the development of this project, I did not fully commited to use all functionalities, since I preferred to 
  keep moving prototype-based as opposed to the idea of applying all best practices. That being said, it means I was not 
  implementing <b class="styledPhrases"> TypeScript</b> and <b class="styledPhrases"> GraphQL</b> even I started 
  an emtpy <b class="styledPhrases"> Next.js</b> template with ready to use support for them intentionally.`,
  `As of now, my main concern is changing between updating the portfolio items and improving the components that&apos;s 
  been displayed. From time to time, I also spend time refactoring all static parts via migrating them to graphQL API 
  while enhancing components with TypeScript.`,
];