export const eventData = [
  {
    img: "/img/portfolio/event/async-javascript.png",
    title: "Asynchronous JavaScript",
    alt: "GitLab Istanbul event flyer titled Asynchronous JavaScript",
    description: "GitLab Istanbul event hosted @ 01/11/2024",
    url: "",
    featured: true,
  },
  {
    img: "/img/portfolio/presentation/generative-ai.png",
    title: "Art & AI: Generative Art from Conventional to AI",
    alt: "Art & AI: Generative Art from Conventional to AI",
    description: "Tech event held by GDSC DEU @ 16/03/2024",
    url: "",
  },
  {
    img: "/img/portfolio/event/snap-ar.jpeg",
    title: "SNAP AR: How to make AI filter via SnapChat?",
    alt: "SNAP AR: How to make AI filter via SnapChat?",
    description: "Meet up hosted by GitLab Istanbul @ 22/02/2024",
    url: "",
  },
  {
    img: "/img/portfolio/presentation/nextjs.jpg",
    title: "Next.js",
    alt: "Next.js",
    description: "Tech conference held by GDSC UTAA @ 22/12/2023",
    url: "",
  },
  {
    img: "/img/portfolio/presentation/react-201.png",
    title: "React 201: Intermediate Level React and Performance Optimization",
    alt: "React 201: Intermediate Level React and Performance Optimization",
    description:
      "Meet up co-hosted by GitLab Istanbul, IBB Veri Laboratuarı & Sisters Lab @ 17/07/2023",
    url: "https://www.youtube.com/watch?v=qAWgKddDp9w",
  },
  {
    img: "/img/portfolio/presentation/react-101.png",
    title: "React 101",
    alt: "Intro for React ecosystem, state management and hooks explained",
    description:
      "Meet up co-hosted by GitLab Istanbul, IBB Veri Laboratuarı & Sisters Lab @ 01/06/2023",
    url: "https://www.youtube.com/watch?v=-40kxE9J7Rw",
  },
  {
    img: "/img/portfolio/event/yazilimci-sohbetleri.png",
    title: "Yazılımcı Sohbetleri",
    description: "Meet up co-hosted by IBB Veri Laboratuarı & Sisters Lab @ 15/12/2022",
    url: "",
  },
  {
    img: "/img/portfolio/event/0004.png",
    title: "Cross Platform UX Design",
    description: "Meet up hosted by GitLab Istanbul @ 17/10/2022",
    url: "",
  },
  {
    img: "/img/portfolio/event/web-sitesi-hazirlama-egitimi.png",
    title: "Development of a Website with HTML, CSS & Js",
    description: "Meet up co-hosted by IBB Veri Laboratuarı & Sisters Lab @ 14/10/2022",
    url: "",
  },
  {
    img: "/img/portfolio/event/0003.png",
    title: "UX for Developers",
    description: "Meet up hosted by GitLab Istanbul @ 15/12/2021",
    url: "https://youtu.be/tsZ9YsuPPYk",
  },
  {
    img: "/img/portfolio/event/0002.png",
    title: "CI/CD with GitLab",
    description: "Meet up hosted by GitLab Istanbul @ 22/11/2021",
    url: "https://youtu.be/F6x74_vUcLo",
  },
  {
    img: "/img/portfolio/event/0001.png",
    title: "Celebrating 10 Years of GitLab in Istanbul",
    description: "Meet up hosted by GitLab Istanbul @ 15/10/2021",
    url: "https://youtu.be/WfT5IW_xNxg",
  },
];