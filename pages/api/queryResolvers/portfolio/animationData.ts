export const animationData = [
  {
    img: "/img/portfolio/creativity/animation/mutfakta_neler_oluyor.png",
    title: "Mutfakta Neler Oluyor?",
    description:
      "A stop-motion Turkish tongue twister created during master studies, Jun 2014",
    url: "https://www.youtube.com/watch?v=fef0efsblmk",
    featured: true,
  },
  {
    img: "/img/portfolio/creativity/animation/msgsu_guestinfo.png",
    title: "MSGSU Guest Information Application",
    description:
      "A keynote to simulate kiosk at the university during master studies, Jun 2013",
    url: "https://www.icloud.com/keynote/0f5kSkodT4q-nQix0Y-xEh6KA#MSGSU%CC%88_Bomonti_Kampu%CC%88su%CC%88_Misafir_Bilgilendirme_Uygulamas%C4%B1",
  },
];