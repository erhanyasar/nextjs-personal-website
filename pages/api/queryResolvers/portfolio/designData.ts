export const designData = [
  {
    img: "/img/portfolio/design/upatient_sketch.png",
    title: "Upatient landing page wireframe",
    description: "",
    url: "",
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: "/img/portfolio/design/upatient_sketch.jpg",
    title: "Upatient landing page sketch",
    description: "",
    url: "",
  },
  {
    img: "/img/portfolio/design/aretemobile_sketch.png",
    title: "Arete Mobile Application",
    description: "",
    url: "",
  },
];