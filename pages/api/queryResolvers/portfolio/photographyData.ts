export const photographyData = [
  {
    img: "/img/portfolio/photography/IMG_9444.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9439.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9428.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9408.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9386.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9373.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9345.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9299.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9294.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9292.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9265.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9264.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9251.JPG",
    title: "A trip to Çitak",
    description: "Dec 2015 @ Van, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9169.JPG",
    title: "Random shooting around Kadikoy",
    description: "Dec 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9156.JPG",
    title: "Random shooting around Kadikoy",
    description: "Dec 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9065.JPG",
    title: "First nude shooting with a friend",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9062.JPG",
    title: "First nude shooting with a friend",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9049.JPG",
    title: "First nude shooting with a friend",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_9040.JPG",
    title: "First nude shooting with a friend",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
  /*
  {
    img: '/img/portfolio/photography/IMG_9044.JPG',
    title: 'First nude shooting with a friend',
    description: 'Sep 2015 @ Istanbul, Turkey'
  },
  */
  {
    img: "/img/portfolio/photography/IMG_9013.JPG",
    title: "First nude shooting with a friend",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/photography/IMG_8994.JPG",
    title: "Random shooting around Cihangir",
    description: "Sep 2015 @ Istanbul, Turkey",
  },
];