export const fullstackData = [
  {
    img: "/img/portfolio/development/to-dos-application.png",
    title: "To-Dos Application",
    description: "To-Dos List & Items w/ Next.js, TypeScript, GraphQL, Jest @ Aug 2023",
    gitURL: "https://gitlab.com/erhanyasar/to-dos-application",
    path: "https://to-dos-app-erhanyasar.vercel.app",
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: "/img/portfolio/development/openai-randezvous-chatbot.png",
    title: "OpenAI Randezvous Chatbot",
    description:
      "Uses natural language processing via OpenAI's GPT-3.5 model, w/ Next.js, TypeScript & @ Jul 2023",
    path: "https://openai-rendezvous-chatbot.vercel.app/",
    gitURL: "https://gitlab.com/erhanyasar/openai-rendezvous-chatbot",
  },
  {
    img: "/img/portfolio/development/erhanyasar-com-tr.png",
    title: "erhanyasar.com.tr",
    description: "Next.js, TypeScript, GraphQL, MUI, Styled Components, Sentry @ Apr 2023",
    gitURL: "https://gitlab.com/erhanyasar.com.tr",
  },
  {
    img: "/img/portfolio/development/testimonialtree.png",
    title: "Testimonial Tree",
    description: "C#, React, TypeScript, Next.js, MUI, Tailwind between 2021 - 2023",
    path: "https://testimonialtree.com/login.aspx",
  },
  {
    img: "/img/portfolio/development/react-job-list.png",
    title: "REST API Server with Express.js",
    description: "A simple REST API POC @ Feb 2021",
    gitURL: "https://github.com/erhanyasar/react-job-list-express-server",
  },
];