export const xrData = [
  {
    img: "/img/portfolio/xr/referandum.png",
    title: "3D Referandum Campaign via WebXR",
    description: "A-Frame @ Apr 2017",
    path: "http://erhanyasar.com.tr/portfolio/frontend/referandum/index.html",
    gitURL: "",
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: "",
    title: "Monster Math Island",
    description: "Tutorial implementation with Unity @ Sep 2022",
    gitURL: "https://github.com/erhanyasar/MonsterMathIsland",
  },
];