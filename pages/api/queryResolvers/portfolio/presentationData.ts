export const presentationData = [
  {
    img: "/img/portfolio/event/async-javascript.png",
    title: "Asynchronous JavaScript",
    alt: "GitLab Istanbul event flyer titled Asynchronous JavaScript",
    description: "GitLab Istanbul event hosted @ 01/11/2024",
    url: "https://www.icloud.com/keynote/0a1oKOggCEe44_KZDMdTX2xKw#Asynchronous_Javascript",
    featured: true,
  },
  {
    img: "/img/portfolio/presentation/generative-ai.png",
    title: "Art & AI: Generative Art from Conventional to AI",
    alt: "Art & AI: Generative Art from Conventional to AI",
    description: "Tech event held by GDSC DEU @ 16/03/2024",
    url: "https://www.icloud.com/keynote/0cf3Whi6z7fqMEgzOUVKgow1g#AI_%26_Art",
  },
  {
    img: "/img/portfolio/presentation/nextjs.jpg",
    title: "Next.js",
    alt: "Next.js",
    description: "Tech event held by Turkish Aviation Academy @ 22/12/2023",
    url: "https://www.icloud.com/keynote/0feMB0yf0jI44WqgIpMDqBw3Q#Nextjs",
  },
  {
    img: "/img/portfolio/presentation/react-201.png",
    title: "React 201: Intermediate Level React and Performance Optimization",
    alt: "React 201: Intermediate Level React and Performance Optimization",
    description:
      "Meet up co-hosted by GitLab Istanbul, IBB Veri Laboratuarı & Sisters Lab @ 15/06/2023",
    url: "https://www.icloud.com/keynote/0f58ag6Xd5QyZ6eDDhpIldojA#React-201",
  },
  {
    img: "/img/portfolio/presentation/react-101.png",
    title: "React 101",
    alt: "Intro for React ecosystem, state management and hooks explained",
    description:
      "Meet up co-hosted by GitLab Istanbul, IBB Veri Laboratuarı & Sisters Lab @ 15/06/2023",
    url: "https://www.icloud.com/keynote/0c5cWjc5oCg5OV1uYwcBJ3fqw#React-101",
  },
  {
    img: "/img/portfolio/lecturing/asenkron-js.png",
    title: "Asenkron Javascript",
    description: "Tutoring Demo for Bilge Adam Teknoloji @ May 2023",
    url: "https://www.icloud.com/keynote/0b5H3FoaKWlc36-Wkf4wN_WIg#Asenkron_Javascript",
  },
  {
    img: "/img/portfolio/presentation/introducing-web-components-with-react&vite.png",
    title: "Introducing Web Components with React & Vite",
    description: "Meet up for Intellectsoft frontend team @ 24/05/2022",
    url: "https://www.icloud.com/keynote/012NgSMtI2_zBUJ2fEaa-FX7w#Introducing_Web_Components_with_React_%26_Vite",
  },
  {
    img: "/img/portfolio/event/web-sitesi-hazirlama-egitimi.png",
    title: "Development of a Website with HTML, CSS & Js @ 14/10/2022",
    description: "Online meet up hosted by IBB Veri Laboratuarı & Sisters Lab",
    url: "",
  },
  {
    img: "/img/portfolio/event/0003.png",
    title: "0003 - UX for Developers",
    description: "GitLab Istanbul meet up @ 15/12/2021",
    url: "https://www.icloud.com/keynote/0c8DhQGp042fRNvO95AryiEbQ#0003%5FUX_for_Developers",
  },
  {
    img: "/img/portfolio/event/0002.png",
    title: "0002 - CI/CD with GitLab",
    description: "GitLab Istanbul meet up @ 22/11/2021",
    url: "https://www.icloud.com/keynote/0c1Z9YkJPz2AIV5kxfwg8uNxg#0002%5FCI%2FCD_with_GitLab",
  },
  {
    img: "/img/portfolio/event/0001.png",
    title: "0001 - Celebrating 10 Years of GitLab in Istanbul",
    description: "GitLab Istanbul meet up @ 15/10/2021",
    url: "https://www.icloud.com/keynote/0ddbhsWMufuPleKlPLPwHTK5g#0001%5FCelebrating_10_Years_of_GitLab_in_Istanbul",
  },
  {
    img: "/img/portfolio/presentation/frontend-development-tools.png",
    title: "Frontend Development Tools",
    description: "Frontend Camp @ Aug 2017",
    url: "https://www.icloud.com/keynote/09coJO3sXUj45dHQwNsE1j-4g#Frontend_Gelis%CC%A7tirme_Arac%CC%A7lar%C4%B1",
  },
];