export const publicationData = [
  {
    img: "/img/portfolio/lecture/ilk-makale.png",
    title: "Personalization of Web Sites",
    description: "Article during master studies, published in Jan 2014",
    url: "https://www.academia.edu/9606205/Web_Sitelerinin_Ki%C5%9Fiselle%C5%9Ftirilmesi_Personalization_of_Web_Sites",
    rows: 2,
    cols: 2,
    featured: true,
  },
  {
    img: "/img/portfolio/lecture/yuksek-lisans-tez.png",
    title: "Measuring Cognitive Friction on Mobile E-Commerce Applications",
    description: "Master Thesis, published in July 2020",
    url: "https://www.academia.edu/99393034/Mobil_E_Ticaret_Uygulamalar%C4%B1nda_Bili%C5%9Fsel_K%C4%B1r%C4%B1l%C4%B1m%C4%B1n_%C3%96l%C3%A7%C3%BClmesi_pdf",
  },
];