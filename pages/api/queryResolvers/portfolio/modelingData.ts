export const modelingData = [
  {
    img: "/img/portfolio/modeling/IMG_4350.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4349.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4348.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4346.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4345.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4344.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4342.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4341.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4339.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4337.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4336.JPG",
    title: "Freestyle shooting at home, Yeldeğirmeni",
    description: "Nov 2022 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/878f7647-5ee0-4a1d-841b-6df7c7a57b4f.jpg",
    title: "While fishing around Bosphorus",
    description: "Nov 2021 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/e0cdd27b-a0e4-4c18-a128-51fd05c2c345.jpg",
    title: "While fishing around Bosphorus",
    description: "Nov 2021 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/aa2a9a92-98b4-417f-9d87-8fd89998bce0.jpg",
    title: "While fishing around Bosphorus",
    description: "Nov 2021 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/7e7248c3-4659-4e51-ad5e-26e2f771de65.jpg",
    title: "While fishing around Bosphorus",
    description: "Nov 2021 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_0207.JPG",
    title: "While clubbing @ Roxy",
    description: "Jan 2015 @ Antalya, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_1741.JPG",
    title: "While clubbing @ Topless",
    description: "Jul 2014 @ Istanbul, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_4061.JPG",
    title: "During a holiday in Kaş",
    description: "Jul 2014 @ Antalya, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_1739.JPG",
    title: "During a holiday in Kaş",
    description: "Jul 2014 @ Antalya, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_0204.JPG",
    title: "During a holiday in Kaş",
    description: "Jul 2014 @ Antalya, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_1414.JPG",
    title: "Randomly taken during a festival",
    description: "May 2014 @ Antalya, Turkey",
  },
  {
    img: "/img/portfolio/modeling/IMG_0113.JPG",
    title: "Randomly taken during a festival",
    description: "May 2014 @ Antalya, Turkey",
  },
];