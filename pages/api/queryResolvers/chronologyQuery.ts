export const chronologyQuery = [
  {
    date: "1985",
    topic: "Born",
    detail: "in Istanbul, Turkey",
  },
  {
    date: "1991",
    topic: "Primary school",
  },
  {
    date: "1996",
    topic: "Middle School",
  },
  {
    date: "1999",
    topic: "Naval High School",
    detail: "Enlisted Navy as a cadet",
  },
  {
    date: "2003",
    topic: "Naval Academy",
    detail: "Computer Engineering & Naval Science",
  },
  {
    date: "Aug 2007 - Dec 2013",
    topic: "Turkish Naval Forces",
    detail: "Graduated as a Navy Ensign & Computer Engineer",
  },
  {
    date: "Sep 2007 - Jun 2008",
    topic: "Intern Officer",
  },
  {
    date: "Jun 2008 - Jul 2011",
    topic: "TCG YILDIRIM (F-243) Underwater Weapons Officer",
  },
  {
    date: "Jul 2011 - Mar 2013",
    topic: "TCG KEMALREİS (F-247) Anti-Submarine Warfare Officer",
  },
  {
    date: "Mar - Dec 2013",
    topic: "North Task Group Command Headquarters Stuff",
  },
  {
    date: "2013 - 2020",
    topic: "Mimar Sinan University of Fine Arts",
    detail: "MSc. in Computational Art & Design",
  },
  {
    date: "Jul 2014 - Jun 2016",
    topic: "Freelancing",
    detail: "Frontend Developer",
  },
  {
    date: "Jun 2016 - Jun 2017",
    topic: "Arete Consulting",
    detail: "Frontend Web Developer (On-site / Istanbul, TR)",
  },
  {
    date: "Oct 2017 - Sep 2018",
    topic: "Related Digital",
    detail: "Frontend Developer (On-site / Istanbul, TR)",
  },
  {
    date: "Aug 2020 - Mar 2021",
    topic: "Devtagon",
    detail: "Frontend Architect (Hybrid / Istanbul, TR)",
  },
  {
    date: "Jun 2021 - Feb 2023",
    topic: "Testimonial Tree",
    detail: "Senior Frontend Developer (Remote / Florida, US)",
  },
  {
    date: "Oct 2021 - Current",
    detail: "<b>Founder, Community Lead</b> @ <b>GitLab Istanbul</b>",
  },
  {
    date: "Nov 2021 - Apr 2022",
    detail: "<b>Software Instructor</b> @ <b>Bilişim Education Center</b> - Overemployed",
  },
  {
    date: "Apr - July 2022",
    detail: "<b>Senior Frontend Developer</b> @ <b>Intellectsoft</b> (US, Remote) - Overemployed",
  },
  {
    date: "Jun 2022 - Nov 2023",
    detail: "<b>Mentor</b> @ <b>Sisters Lab</b>",
  },
  {
    date: "Apr - Oct 2023",
    detail: "<b>Senior Frontend Developer</b> @ <b>Disaster Accountability Project</b>",
  },
  {
    date: "July 2023 - Current",
    detail: "<b>Mentor</b> @ <b>Magnet Mode Team</b>",
  },
  {
    date: "Aug 2023 - Jan 2024",
    detail: "<b>Mentor</b> @ <b>Re:Coded</b>",
  },
  {
    date: "Oct 2023 - Apr 2024",
    topic: "Stealth Startup",
    detail: "Frontend Team Lead (UK, Remote)",
  },
  {
    date: "Jan 2024 - Current",
    detail: "<b>Mentor</b> @ <b>Tech Istanbul</b>",
  },
  {
    date: "Apr - Sep 2024",
    topic: "Palwise AI",
    detail: "Technical Co-Founder",
  },
  {
    date: "May - Sep 2024",
    detail: "<b>Software Instructor</b> @ <b>Acunmedya Akademi</b>",
  },
  {
    date: "Oct 2024 - Current",
    topic: "Compunart",
    detail: "Co-Founder",
  }
];