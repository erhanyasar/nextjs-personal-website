export const userQuery = {
  info: `Hello!<br/>I'm Erhan Yaşar,<br/><span style='color: #ef5285;'>
      Frontend Architect</span><br/>living in Istanbul, Turkey.<br/><span style='font-size: 3rem;'><span style='color: #379398;'>MSc</span> | <span style='color: #379398;'>
      Software Instructor</span> | <span style='color: #379398;'>Community Lead</span> | <span style='color: #379398;'>Mentor</span></span>`,
  title: "Erhan Yaşar | MSc., Frontend Architect, Software Instructor, Community Lead, Mentor",
  aboutMe: [
    {
      text: "Experienced software engineer with a unique background in naval service, transitioning to design and development.",
      icon: "🏄‍♂️",
    },
    {
      text: "Adept at architecting user interfaces for immersive digital experiences.",
      icon: "🎯",
    },
    {
      text: "Over nine years of expertise in React, Next, TypeScript, GraphQL, AWS, Jest, and more.",
      icon: "🧙‍♂️",
    },
    {
      text: "Passionate about tutoring, mentorship, and contributing to WebXR projects.",
      icon: "🧘‍♂️",
    },
  ],
};