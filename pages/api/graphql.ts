import { createSchema, createYoga } from "graphql-yoga";
import { chronologyQuery } from "./queryResolvers/chronologyQuery";
import { contactQuery } from "./queryResolvers/contactQuery";
import { portfolioQuery } from "./queryResolvers/portfolioQuery";
import { referencesQuery } from "./queryResolvers/referencesQuery";
import { userQuery } from "./queryResolvers/userQuery";
import { welcomeDialogQuery } from "./queryResolvers/welcomeDialogQuery";
import { typeDefs } from "./typeDefs";

const resolvers = {
  Query: {
    welcomeDialog() {
      return welcomeDialogQuery;
    },
    user() {
      return userQuery;
    },
    chronology() {
      return [...chronologyQuery];
    },
    portfolio() {
      return portfolioQuery;
    },
    references() {
      return [...referencesQuery];
    },
    contact() {
      return contactQuery;
    },
  },
};

const schema = createSchema({
  typeDefs,
  resolvers,
});

export const config = {
  api: {
    bodyParser: false, // Disable body parsing (required for file uploads)
  },
};

export default createYoga({
  schema,
  // Needed to be defined explicitly because our endpoint lives at a different path other than `/graphql`
  graphqlEndpoint: "/api/graphql",
});
