export const typeDefs = /* GraphQL */ `
  type Query {
    welcomeDialog: [String]
    user: User!
    chronology: [Chronology!]
    portfolio: Portfolio
    references: [Reference!]!
    contact: Contact
  }

  type User {
    info: String
    title: String
    aboutMe: [AboutMe]
  }

  type AboutMe {
    text: String
    icon: String
  }

  type Chronology {
    date: String
    topic: String
    detail: String
  }

  type Portfolio {
    educativeData: [FrontendData!]
    frontendData: [FrontendData!]
    fullstackData: [FullstackData!]
    generativeArtData: [GenerativeArtData!]
    xrData: [XRData!]
    eventData: [EventData!]
    presentationData: [PresentationData!]
    designData: [DesignData!]
    animationData: [AnimationData!]
    publicationData: [PublicationData!]
    photographyData: [PhotographyData!]
    modelingData: [ModelingData!]
  }

  type FrontendData {
    img: String
    title: String
    description: String
    path: String
    gitURL: String
    path2: String
    pathTitle: String
    path2Title: String
    rows: Int
    cols: Int
    featured: Boolean
  }

  type FullstackData {
    img: String
    title: String
    description: String
    gitURL: String
    path: String
    rows: Int
    cols: Int
    featured: Boolean
  }

  type GenerativeArtData {
    img: String
    title: String
    description: String
    path: String
    gitURL: String
    featured: Boolean
  }

  type XRData {
    img: String
    title: String
    description: String
    path: String
    gitURL: String
    rows: Int
    cols: Int
    featured: Boolean
  }

  type EventData {
    img: String
    title: String
    alt: String
    description: String
    url: String
    featured: Boolean
  }

  type PresentationData {
    img: String
    title: String
    alt: String
    description: String
    url: String
    featured: Boolean
  }

  type DesignData {
    img: String
    title: String
    description: String
    url: String
    rows: Int
    cols: Int
    featured: Boolean
  }

  type AnimationData {
    img: String
    title: String
    description: String
    url: String
    featured: Boolean
  }

  type PublicationData {
    img: String
    title: String
    description: String
    url: String
    rows: Int
    cols: Int
    featured: Boolean
  }

  type PhotographyData {
    img: String
    title: String
    description: String
  }

  type ModelingData {
    img: String
    title: String
    description: String
  }

  type Reference {
    src: String!
    title: String!
    URL: String
  }

  type Contact {
    phone: String
    email: String
    linkedin: String
    gitlab: String
    github: String
    stackoverflow: String
    stackexchange: String
    behance: String
    instagram: String
    facebook: String
    twitter: String
  }
`;