import useSWR from "swr";
import { Data } from "../../src/types/sections";
import { fetcher } from "../../src/utils/helpers/fetcher";

export const useUserInfo = () => {
  const { data, error, isLoading } = useSWR<Data>(
    `{
      welcomeDialog,
      user { info, title, aboutMe { text, icon } },
      chronology { date, topic, detail },
      references { src, title, URL },
      contact { phone, email, linkedin, gitlab, github, stackoverflow, stackexchange, behance, instagram, facebook, twitter },
    }`,
    fetcher
  );

  return {
    data,
    error,
    isLoading,
  };
};

export const usePortfolio = () => {
  const { data, error, isLoading } = useSWR<Data>(
    `{
        portfolio {
          educativeData { img, title, description, path, gitURL, path2, pathTitle, path2Title, rows, cols, featured },
          frontendData { img, title, description, path, gitURL, path2, pathTitle, path2Title, rows, cols, featured },
          fullstackData { img, title, description, gitURL, path, rows, cols, featured },
          generativeArtData { img, title, description, path, gitURL, featured },
          xrData { img, title, description, path, gitURL, rows, cols, featured },
          eventData { img, title, alt, description, url, featured },
          presentationData { img, title, alt, description, url, featured },
          designData { img, title, description, url, rows, cols, featured },
          animationData { img, title, description, url, featured },
          publicationData { img, title, description, url, rows, cols, featured },
          photographyData { img, title, description },
          modelingData { img, title, description },
        },
      }`,
    fetcher
  );

  return {
    data,
    error,
    isLoading,
  };
};